`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:35:01 07/14/2015 
// Design Name: 
// Module Name:    edgeDetector 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module edgeDetector(
    input wire signal,
    output wire edges
    );

wire delayedSignal;

//delay #("ED")delta (!signal,delayedSignal);
delay delta (!signal,delayedSignal);

assign edges = delayedSignal&signal;


endmodule

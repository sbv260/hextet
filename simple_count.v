`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:40:46 05/12/2008 
// Design Name: 	 
// Module Name:    simple_count 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module simple_ticker(
    input clock,
    input rst,
    output reg tick
    );

   reg [20:0] count = 0;
	wire [5:0] high, low;
	wire trigger;
	
	assign high = count [20:17];
	assign low = count [5:0];
	assign trigger = count[20]&count[19]&count[18]&count[17]&count[16]&count[15]&count[14]&count[13]&count[12]&count[11]&count[10]&count[09]&count[08]&count[07]&count[06]&count[05];
   always @(posedge clock,posedge rst) begin
      if (rst) begin
			count <= 0;
		end else begin
         count <= count + 1;
		end
	end
	
	
	
	always @(high) begin
		if (high >= 6'b111101) begin
			tick = 1;
		end else begin
			tick = 0;
		end
	end

endmodule

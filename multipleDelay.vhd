----------------------------------------------------------------------
-- Company: Universidad de Guadalajara
-- Engineer: Ing. Sergio Barrios
-- 
-- Create Date:    12:58:05 03/19/2015 
-- Design Name:    Retraso de duracion configurable
-- Module Name:    multipleDelay - Behavioral 
-- Project Name:   RDN
-- Target Devices: Spartan 3E
-- Tool versions: 
-- Description:    Instancia una cantidad configurable de retrasos.
--
-- Dependencies:   transportDelay
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity multipleDelay is
	 Generic (N : integer := 1;
				 X : integer := 150--;
				 --USET : string := "0");
				 );
    Port ( D : in  STD_LOGIC;
           Dout : out  STD_LOGIC);
end multipleDelay;

architecture Behavioral of multipleDelay is
component transportDelay is
	 Generic (X : integer := 150;
				 Y : integer := 0--;
				 --USET : string := "0");
				 );
    Port ( D : in  STD_LOGIC;
           Dout : inout  STD_LOGIC);
end component;
signal D_i : STD_LOGIC_VECTOR (0 TO N-1);
begin
GEN_DEL: for I in 0 to N-1 generate
		GENIF_DEL0: if I=0 generate
			--DEL0: transportDelay generic map (X,I,USET) port map (D,D_i(I));
			DEL0: transportDelay generic map (X,I) port map (D,D_i(I));
		end generate GENIF_DEL0;
		GENIF_DELX: if I>0 generate
			--DELX: transportDelay generic map (X,I,USET) port map (D_i(I-1),D_i(I));
			DELX: transportDelay generic map (X,I) port map (D_i(I-1),D_i(I));
		end generate GENIF_DELX;
end generate GEN_DEL;
Dout <= D_i(N-1);
end Behavioral;


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:01:10 11/01/2017 
// Design Name: 
// Module Name:    delay_for_network 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module delay_for_network(
    input wire di,
    output wire Do
    );
//parameter uset = "0";
//multipleDelay #(.N(15),.X(15),.USET(uset)) DELA (
multipleDelay #(.N(3),.X(15)) DELA (
           .D(di),
           .Dout(Do));

endmodule

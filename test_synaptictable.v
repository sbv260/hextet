`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:04:23 10/18/2017
// Design Name:   synaptic_table
// Module Name:   C:/Users/JRobert/Documents/Xilinx/AER_8bit_zynq/AER_8bit/test_synaptictable.v
// Project Name:  AER_8bit
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: synaptic_table
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_synaptictable;

	// Inputs
	reg [3:0] AER_BUS_IN;
	reg [3:0] addr;
	reg [3:0] data;
	reg we;
	reg clk;
	reg req_in;

	// Outputs
	wire [3:0] AER_BUS_OUT;
	wire req_out;

	// Instantiate the Unit Under Test (UUT)
	synaptic_table uut (
		.AER_BUS_IN(AER_BUS_IN), 
		.AER_BUS_OUT(AER_BUS_OUT), 
		.addr(addr), 
		.data(data), 
		.we(we), 
		.clk(clk),
		.req_in(req_in),
		.req_out(req_out)
	);
	
   always begin
	#41 clk = !clk;
	end

	initial begin
		// Initialize Inputs
		AER_BUS_IN = 0;
		addr = 0;
		data = 0;
		we = 0;
		clk = 0;
		req_in = 0;
		// Wait 100 ns for global reset to finish
		#100;
		AER_BUS_IN = 3;
		req_in = 1;
		#3;
		AER_BUS_IN = 0;
		req_in = 0;
		#100
		addr = 3;
		data = 1;
		#10
		we = 1;
		#100
		we = 0;
		#100
      AER_BUS_IN = 3;
		req_in = 1;		
		#3;
		AER_BUS_IN = 0;
		req_in = 0;
		// Add stimulus here

	end
      
endmodule


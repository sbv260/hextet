`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company: 		Universidad de Guadalajara
// Engineer: 		Ing. Sergio Barrios
// 
// Create Date:    13:59:06 07/06/2015 
// Design Name:	   Celdas de Memoria
// Module Name:    pipelineCells 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:    Shift Register en ambas direcciones
//				 con limites en los bordes.
// Dependencies: Ninguna. 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module pipelineCells (clk,wen,en,direction,dataIn,dataOut);
	parameter n = 16;
   input wire clk;
	input wire wen;
	input wire en;
	input wire direction;
	input wire [n-1:0] dataIn;
   output reg [n:0] dataOut;
	
	//initialize with one 1 and the rest 0s
	initial dataOut = {1'b0,{n-1{1'b0}},1'b1}; 
	
	always@(posedge clk, posedge wen) begin
		if (wen) begin
			dataOut = {1'b0,dataIn};
		end else if (en) begin
			if (direction&&(!dataOut[n])) begin
				dataOut = {dataOut[n-1:0],1'b0};
			end else if (!direction&&(!dataOut[0])) begin
				dataOut = {1'b0,dataOut[n:1]};
			end
		end else begin
			dataOut = dataOut;
		end
	end
endmodule

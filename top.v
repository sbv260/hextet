`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:36:29 09/13/2017 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
input wire clk,
input wire rst,
input wire [15:0] Conf_Reg,
input wire [3:0]  neuron_index,
input wire cen,
input wire [3:0] addr,
input wire [6:0] data,
input wire we,
output wire [3:0] AER_databus_output,
output wire R1_output,
input wire S,
output wire Y/*,
output wire R0_output,
output wire [15:0] data_channels_2_output*/
    );
   

wire A,R0,R1;
wire [3:0] AER_databus0;
wire [6:0] AER_databus1;
wire [15:0] data_channels;
wire [127:0] data_channels_2;
wire [15:0] clk_array;

wire [15:0] Conf_Reg_0;
wire [15:0] Conf_Reg_1;
wire [15:0] Conf_Reg_2;
wire [15:0] Conf_Reg_3;
wire [15:0] Conf_Reg_4;
wire [15:0] Conf_Reg_5;
wire [15:0] Conf_Reg_6;
wire [15:0] Conf_Reg_7;
wire [15:0] Conf_Reg_8;
wire [15:0] Conf_Reg_9;
wire [15:0] Conf_Reg_10;
wire [15:0] Conf_Reg_11;
wire [15:0] Conf_Reg_12;
wire [15:0] Conf_Reg_13;
wire [15:0] Conf_Reg_14;
wire [15:0] Conf_Reg_15;
wire [15:0] cen_array;

delay_network delnet (clk,clk_array);

Conf_Reg_mux mux ({cen,Conf_Reg},neuron_index,{cen_array[0],Conf_Reg_0},{cen_array[1],Conf_Reg_1},{cen_array[2],Conf_Reg_2},{cen_array[3],Conf_Reg_3},{cen_array[4],Conf_Reg_4},{cen_array[5],Conf_Reg_5},{cen_array[6],Conf_Reg_6},{cen_array[7],Conf_Reg_7},{cen_array[8],Conf_Reg_8},{cen_array[9],Conf_Reg_9},{cen_array[10],Conf_Reg_10},{cen_array[11],Conf_Reg_11},{cen_array[12],Conf_Reg12},{cen_array[13],Conf_Reg_13},{cen_array[14],Conf_Reg_14},{cen_array[15],Conf_Reg_15});

RotateDigitalNeuron n0 (clk_array[0],rst,cen_array[0],Conf_Reg_0,data_channels_2[0],data_channels_2[1],data_channels_2[2],data_channels_2[3],data_channels_2[4],data_channels_2[5],data_channels[0],A);
RotateDigitalNeuron n1 (clk_array[1],rst,cen_array[1],Conf_Reg_1,data_channels_2[8],data_channels_2[9],data_channels_2[10],data_channels_2[11],data_channels_2[12],data_channels_2[13],data_channels[1],A);
RotateDigitalNeuron n2 (clk_array[2],rst,cen_array[2],Conf_Reg_2,data_channels_2[16],data_channels_2[17],data_channels_2[18],data_channels_2[19],data_channels_2[20],data_channels_2[21],data_channels[2],A);
RotateDigitalNeuron n3 (clk_array[3],rst,cen_array[3],Conf_Reg_3,data_channels_2[24],data_channels_2[25],data_channels_2[26],data_channels_2[27],data_channels_2[28],data_channels_2[29],data_channels[3],A);
RotateDigitalNeuron n4 (clk_array[4],rst,cen_array[4],Conf_Reg_4,data_channels_2[32],data_channels_2[33],data_channels_2[34],data_channels_2[35],data_channels_2[36],data_channels_2[37],data_channels[4],A);
RotateDigitalNeuron n5 (clk_array[5],rst,cen_array[5],Conf_Reg_5,data_channels_2[40],data_channels_2[41],data_channels_2[42],data_channels_2[43],data_channels_2[44],data_channels_2[45],data_channels[5],A);
RotateDigitalNeuron n6 (clk_array[6],rst,cen_array[6],Conf_Reg_6,data_channels_2[48],data_channels_2[49],data_channels_2[50],data_channels_2[51],data_channels_2[52],data_channels_2[53],data_channels[6],A);
RotateDigitalNeuron n7 (clk_array[7],rst,cen_array[7],Conf_Reg_7,data_channels_2[56],data_channels_2[57],data_channels_2[58],data_channels_2[59],data_channels_2[60],data_channels_2[61],data_channels[7],A);
RotateDigitalNeuron n8 (clk_array[8],rst,cen_array[8],Conf_Reg_8,data_channels_2[64],data_channels_2[65],data_channels_2[66],data_channels_2[67],data_channels_2[68],data_channels_2[69],data_channels[8],A);
RotateDigitalNeuron n9 (clk_array[9],rst,cen_array[9],Conf_Reg_9,data_channels_2[72],data_channels_2[73],data_channels_2[74],data_channels_2[75],data_channels_2[76],data_channels_2[77],data_channels[9],A);
RotateDigitalNeuron n10 (clk_array[10],rst,cen_array[10],Conf_Reg_10,data_channels_2[80],data_channels_2[81],data_channels_2[82],data_channels_2[83],data_channels_2[84],data_channels_2[85],data_channels[10],A);
RotateDigitalNeuron n11 (clk_array[11],rst,cen_array[11],Conf_Reg_11,data_channels_2[88],data_channels_2[89],data_channels_2[90],data_channels_2[91],data_channels_2[92],data_channels_2[93],data_channels[11],A);
RotateDigitalNeuron n12 (clk_array[12],rst,cen_array[12],Conf_Reg_12,data_channels_2[96],data_channels_2[97],data_channels_2[98],data_channels_2[99],data_channels_2[100],data_channels_2[101],data_channels[12],A);
RotateDigitalNeuron n13 (clk_array[13],rst,cen_array[13],Conf_Reg_13,data_channels_2[104],data_channels_2[105],data_channels_2[106],data_channels_2[107],data_channels_2[108],data_channels_2[109],data_channels[13],A);
RotateDigitalNeuron n14 (clk_array[14],rst,cen_array[14],Conf_Reg_14,data_channels_2[112],data_channels_2[113],data_channels_2[114],data_channels_2[115],data_channels_2[116],data_channels_2[117],data_channels[14],A);
encoder aer_encoder (data_channels,AER_databus0,R0,A);
synaptic_table ST (AER_databus0,AER_databus1,R0,R1,addr,data,we,clk);
decoder aer_decoder (clk,data_channels_2,AER_databus1,R1,A);

assign data_channels [15] = S;
assign Y = data_channels[14];

assign AER_databus_output = AER_databus1;
assign R1_output = R1;
assign neurons_output = data_channels;
/*assign data_channels_2_output = data_channels_2;
assign R0_output = R0;*/
endmodule

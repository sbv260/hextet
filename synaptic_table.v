`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:04:18 10/18/2017 
// Design Name: 
// Module Name:    synaptic_table 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module synaptic_table(
    input wire [3:0] AER_BUS_IN,
    output reg [6:0] AER_BUS_OUT,
	 input wire req_in,
	 output wire req_out,
    input wire [3:0] addr,
    input wire [6:0] data,
    input wire we,
	 input wire clk
    );

reg [6:0] memory_0;
reg [6:0] memory_1;
reg [6:0] memory_2;
reg [6:0] memory_3;
reg [6:0] memory_4;
reg [6:0] memory_5;
reg [6:0] memory_6;
reg [6:0] memory_7;
reg [6:0] memory_8;
reg [6:0] memory_9;
reg [6:0] memory_10;
reg [6:0] memory_11;
reg [6:0] memory_12;
reg [6:0] memory_13;
reg [6:0] memory_14;
reg [6:0] memory_15;

initial begin
	memory_0 = 7'b0000111;
	memory_1 = 7'b0000111;
	memory_2 = 7'b0000111;
	memory_3 = 7'b0000111;
	memory_4 = 7'b0000111;
	memory_5 = 7'b0000111;
	memory_6 = 7'b0000111;
	memory_7 = 7'b0000111;
	memory_8 = 7'b0000111;
	memory_9 = 7'b0000111;
	memory_10 = 7'b0000111;
	memory_11 = 7'b0000111;
	memory_12 = 7'b0000111;
	memory_13 = 7'b0000111;
	memory_14 = 7'b0000111;
	memory_15 = 7'b0000111;
end

always@(AER_BUS_IN) begin
	case (AER_BUS_IN)
	4'b0000: AER_BUS_OUT = memory_0;
	4'b0001: AER_BUS_OUT = memory_1;
	4'b0010: AER_BUS_OUT = memory_2;
	4'b0011: AER_BUS_OUT = memory_3;
	4'b0100: AER_BUS_OUT = memory_4;
	4'b0101: AER_BUS_OUT = memory_5;
	4'b0110: AER_BUS_OUT = memory_6;
	4'b0111: AER_BUS_OUT = memory_7;
	4'b1000: AER_BUS_OUT = memory_8;
	4'b1001: AER_BUS_OUT = memory_9;
	4'b1010: AER_BUS_OUT = memory_10;
	4'b1011: AER_BUS_OUT = memory_11;
	4'b1100: AER_BUS_OUT = memory_12;
	4'b1101: AER_BUS_OUT = memory_13;
	4'b1110: AER_BUS_OUT = memory_14;
	4'b1111: AER_BUS_OUT = memory_15;
	endcase
end

always@(posedge clk) begin
	if (we) begin
		case (addr)
		4'b0000: memory_0=data;
		4'b0001: memory_1=data;
		4'b0010: memory_2=data;
		4'b0011: memory_3=data;
		4'b0100: memory_4=data;
		4'b0101: memory_5=data;
		4'b0110: memory_6=data;
		4'b0111: memory_7=data;
		4'b1000: memory_8=data;
		4'b1001: memory_9=data;
		4'b1010: memory_10=data;
		4'b1011: memory_11=data;
		4'b1100: memory_12=data;
		4'b1101: memory_13=data;
		4'b1110: memory_14=data;
		4'b1111: memory_15=data;
		endcase
	end
end

delaysynaptictable delayreq_synaptictable (req_in,req_out);
endmodule
`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   07:24:40 10/18/2017
// Design Name:   encoder
// Module Name:   C:/Users/JRobert/Documents/Xilinx/AER_8bit_zynq/AER_8bit/test_encoder.v
// Project Name:  AER_8bit
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: encoder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_encoder;

	// Inputs
	reg [15:0] input_channels;
	reg A;

	// Outputs
	wire [3:0] data;
	wire R;

	// Instantiate the Unit Under Test (UUT)
	encoder uut (
		.input_channels(input_channels), 
		.data(data), 
		.R(R), 
		.A(A)
	);

	initial begin
		// Initialize Inputs
		input_channels = 0;
		A = 0;

		// Wait 100 ns for global reset to finish
		#100;
		input_channels = 8;
		#3;
		input_channels = 0;
      #100;
		input_channels = 4;
		#3;
		input_channels = 0;  
		// Add stimulus here

	end
      
endmodule


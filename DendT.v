`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:16:19 01/23/2016 
// Design Name: 
// Module Name:    DendT 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DendT(
    input wire W1,
    input wire W2,
    input wire W3,
    output wire O
    );
wire W2d, W3d, W3dd;
delay weight2delay (W2, W2d);
delay weight3delay1(W3, W3d);
delay weight3delay2(W3d, W3dd);

assign O = W1 || W2 || W2d || W3 || W3d || W3dd;


endmodule

`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:00:48 11/06/2017
// Design Name:   Conf_Reg_mux
// Module Name:   C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/test_mux.v
// Project Name:  AER_8bit
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Conf_Reg_mux
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_mux;

	// Inputs
	reg [16:0] Data_Input;
	reg [3:0] sel;

	// Outputs
	wire [16:0] Data_Output_0;
	wire [16:0] Data_Output_1;
	wire [16:0] Data_Output_2;
	wire [16:0] Data_Output_3;
	wire [16:0] Data_Output_4;
	wire [16:0] Data_Output_5;
	wire [16:0] Data_Output_6;
	wire [16:0] Data_Output_7;
	wire [16:0] Data_Output_8;
	wire [16:0] Data_Output_9;
	wire [16:0] Data_Output_10;
	wire [16:0] Data_Output_11;
	wire [16:0] Data_Output_12;
	wire [16:0] Data_Output_13;
	wire [16:0] Data_Output_14;
	wire [16:0] Data_Output_15;

	// Instantiate the Unit Under Test (UUT)
	Conf_Reg_mux uut (
		.Data_Input(Data_Input), 
		.sel(sel), 
		.Data_Output_0(Data_Output_0), 
		.Data_Output_1(Data_Output_1), 
		.Data_Output_2(Data_Output_2), 
		.Data_Output_3(Data_Output_3), 
		.Data_Output_4(Data_Output_4), 
		.Data_Output_5(Data_Output_5), 
		.Data_Output_6(Data_Output_6), 
		.Data_Output_7(Data_Output_7), 
		.Data_Output_8(Data_Output_8), 
		.Data_Output_9(Data_Output_9), 
		.Data_Output_10(Data_Output_10), 
		.Data_Output_11(Data_Output_11), 
		.Data_Output_12(Data_Output_12), 
		.Data_Output_13(Data_Output_13), 
		.Data_Output_14(Data_Output_14), 
		.Data_Output_15(Data_Output_15)
	);

	initial begin
		// Initialize Inputs
		Data_Input = 0;
		sel = 0;

		// Wait 100 ns for global reset to finish
		#100;
		sel = 4'b1101;
		#100;
      Data_Input [15:0] = 16'b001111111111111; 
		#300;
		Data_Input [16] = 1;
		#30;
		Data_Input [16] = 0;
		// Add stimulus here

	end
      
endmodule


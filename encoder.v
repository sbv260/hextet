`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:32:47 09/13/2017 
// Design Name: 
// Module Name:    sender 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module encoder(
	 input [15:0] input_channels,
    output [3:0] data,
    output R,
    input  A
    );

wire [15:0] input_channels;
reg [3:0] data;
reg R;

initial begin
	data=0;
	R=0;
end

always@(input_channels)
case(input_channels)
	16'b0000000000000001:data=0;
	16'b0000000000000010:data=1;
	16'b0000000000000100:data=2;
	16'b0000000000001000:data=3;
	16'b0000000000010000:data=4;
	16'b0000000000100000:data=5;
	16'b0000000001000000:data=6;
	16'b0000000010000000:data=7;
	16'b0000000100000000:data=8;
	16'b0000001000000000:data=9;
	16'b0000010000000000:data=10;
	16'b0000100000000000:data=11;
	16'b0001000000000000:data=12;
	16'b0010000000000000:data=13;
	16'b0100000000000000:data=14;
	16'b1000000000000000:data=15;
	default:data=0;
endcase

always@(input_channels)
case(input_channels)
	16'b0000000000000001:R=1;
	16'b0000000000000010:R=1;
	16'b0000000000000100:R=1;
	16'b0000000000001000:R=1;
	16'b0000000000010000:R=1;
	16'b0000000000100000:R=1;
	16'b0000000001000000:R=1;
	16'b0000000010000000:R=1;
	16'b0000000100000000:R=1;
	16'b0000001000000000:R=1;
	16'b0000010000000000:R=1;
	16'b0000100000000000:R=1;
	16'b0001000000000000:R=1;
	16'b0010000000000000:R=1;
	16'b0100000000000000:R=1;
	16'b1000000000000000:R=1;
	default:R=0;
endcase
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:45:05 09/13/2017 
// Design Name: 
// Module Name:    mullerC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mullerC(
    input A,
    input B,
    output Y
    );
reg Y_i;
initial begin
	Y_i=0;
end
always @ (A,B)
case ({A,B})
		0:Y_i=1'b0;
		3:Y_i=1'b1;
		default:Y_i=Y_i;
endcase

assign Y = Y_i;
endmodule

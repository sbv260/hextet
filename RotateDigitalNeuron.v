`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company: Universidad de Guadalajara
// Engineer: Ing. Sergio Barrios
// 
// Create Date:    15:28:11 07/06/2015 
// Design Name:    Neurona
// Module Name:    RotateDigitalNeuron 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description: 	Neurona digital de impulsos
//
// Dependencies: delay, dynamicBehaviorSimulator, initialValueSelector,
// 	pipelineCells, spikeGenerator, rotationController, register
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////

// Declaracion con todas senales de salida para buscar errores
//module RotateDigitalNeuron(clk,rst,cen,Ain,S,Y,U,V,state,newstate,du,dv,Uclk,Vclk);


// Declaracion para implementacion final
module RotateDigitalNeuron(clk,rst,cen,Ain,SE1,SE2,SE3,SI1,SI2,SI3,Y,ack);
	parameter n = 16;
   input wire clk;
	input wire rst;
	input wire cen;
	input wire [15:0] Ain;
	wire [15:0] A;
	wire S;
	input wire SE1,SE2,SE3,SI1,SI2,SI3;
	output wire Y;
	input wire ack;
	
	
	wire clk_i,clk_i2;
   wire [1:0] du, dv;
	wire dv_i;
	wire [n-1:0] Un, Vn;
	wire [n:0] U, V;
	wire rstV,Y_rstV, rstU;
	wire Uclk,Vclk;
	wire newstate;
	wire state;
	
	wire Y_i;
	wire isVnotatinitial;
	assign isVnotatinitial = (V[n-1:0] > Vn); 

	
	delay delayV(Y_i|rstV,Y_rstV);
	//assign #1 Y_rstV = Y_i|rstV; //Descripcion no sintetizable
	
	dynamicBehaviorSimulator dyBeSi (rst,clk,U,V,A[15:14],S,state,newstate,rstU,rstV);
	initialValueSelectorU initialU (newstate,A[6:0],Un);
	pipelineCells Ucells (Uclk,rstU,du[0],du[1],Un,U);
	initialValueSelectorV initiaV (newstate,A[13:7],U[n-1:0],Vn);
	pipelineCells Vcells (Vclk,Y_rstV,dv[0],dv_i,Vn,V);
	spikeGenerator spikesG (V,Y_i);
	//rotationController rotCon (clk,clk_i, rst, A[15:14], S, state,U[n-1], V[n-2],isVnotatinitial,du,dv,Uclk,Vclk);
	rotationController rotCon (clk_i,clk_i, rst, A[15:14], S, state,U[n], V[n-1],isVnotatinitial,du,dv,Uclk,Vclk);
	stimuliReceiver stiRec (SE1,SE2,SE3,SI1,SI2,SI3,S,dv[1],dv_i);
	assign Y = Y_i;
	
	register regpacket (clk,cen,Ain,A);
	//simple_ticker clkgen (clk,rst,clk_i2);
	//edgeDetector clkedg (clk_i2,clk_i);
	edgeDetector clkedg (clk,clk_i);
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company: 		Universidad de Guadalajara
// Engineer:        Ing. Sergio Barrios
// 
// Create Date:    17:17:30 10/17/2015 
// Design Name:    Generador de pulsos
// Module Name:    spikeGenerator 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:    Retraso sobre la senal de salida que se
//                  usa como resetal pipeline.
// Dependencies:   Ninguna.
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module spikeGenerator(V,Y);
parameter n =  16;
input wire [n:0] V;
output wire Y;
//edgeDetector edgeDet (V[n-1]|V[n],Y);
assign Y = V[n];

endmodule

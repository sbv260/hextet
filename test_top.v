`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:19:55 10/19/2017
// Design Name:   top
// Module Name:   C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/test_top.v
// Project Name:  AER_8bit
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_top;

	// Inputs
	reg clk;
	reg rst;
	reg [15:0] Conf_Reg;
	reg [3:0]  neuron_index;
	reg cen;
	reg [3:0] addr;
	reg [6:0] data;
	reg we;
	reg S;

	// Outputs
	wire Y;

	// Instantiate the Unit Under Test (UUT)
	top uut (
		.clk(clk), 
		.rst(rst), 
		.Conf_Reg(Conf_Reg), 
		.neuron_index(neuron_index),
		.cen(cen), 
		.addr(addr), 
		.data(data), 
		.we(we), 
		.S(S), 
		.Y(Y)
	);
	
	always begin
	#41 clk = !clk;
	end

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		Conf_Reg = 16'b001111111111111;
		neuron_index = 4'b1110;
		cen = 0;
		addr = 0;
		data = 0;
		we = 0;
		S = 0;
		// Wait 100 ns for global reset to finish
		#100;
		rst = 1;
		#10;
		rst = 0;
		#100;
		cen = 1;
		#300;
		cen = 0;
		#20;
		rst = 1;
		#10;
		rst = 0;
		#10;
		addr = 4'b1111;
		data = 7'b1110000;
		#10;
		we = 1;
		#100;
		we = 0;
		#10;
		addr = 4'b1110;
		data = 7'b1111111;		
		#10;
		we = 1;
		#100;
		we = 0;
		#1000;
		rst = 1;
		#100;
		rst = 0;
		#2000;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;
		S = 1;
		#30;
		S=0;
		#100;

		// Add stimulus here

	end
      
endmodule


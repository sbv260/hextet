`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company:        Universidad de Guadalajara
// Engineer:       Ing. Sergio Barrios
// 
// Create Date:    18:36:40 10/17/2015 
// Design Name:    Controlador de rotacion
// Module Name:    rotationController 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:    Envia las senales de control de las celdas 
//  de memoria basandose en el modo, estado y valores de las celdas.
// Dependencies: Ninguna. 
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
///////////////////////////////////////////////////////////////////////
module rotationController(clk_fast,clk_slow, rst, A, S, state,U,V,isVnotatinitial,du,dv,Uclk,Vclk);
input wire clk_fast;
input wire clk_slow;
input wire rst;
input wire [1:0] A;
input wire S;
input wire state;
input wire U;
input wire V;
input wire isVnotatinitial;
output reg [1:0]du;
output reg [1:0]dv;
output reg Uclk;
output reg Vclk;
wire leak;


initial begin
	du = 0;
	dv = 0;
	Uclk = 0;
	Vclk = 0;
end

assign leak = (U&!dv[1]&isVnotatinitial);

always @ (clk_slow, rst,A, S, state,U,V,dv[1])
begin
	case (state)
		0:begin
			if (!A[0]) begin
				du = 2'b11;
				if (U&!S) begin
					dv = 2'b01;
				end else begin
					dv = 2'b11;
				end
			end else begin
				du = 2'b11;
				dv = 2'b00;	
			end
			Uclk = clk_slow;
			if (rst||A[0]) begin
				Vclk = clk_slow;
			end else begin
				Vclk = S||leak;
			end
		end
		1: begin
			Uclk = clk_slow;
			Vclk = clk_slow||((S)&&(!A[1]));
			dv = 2'b11;
			if (V) begin
				du = 2'b11;
			end else begin
				du = 2'b00;
			end
		end
	endcase
end

endmodule
		
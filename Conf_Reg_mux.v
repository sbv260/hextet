`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:35:55 11/04/2017 
// Design Name: 
// Module Name:    Conf_Reg_mux 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Conf_Reg_mux(
    input wire [16:0] Data_Input,
    input wire [3:0] sel,
    output reg [16:0] Data_Output_0,
	 output reg [16:0] Data_Output_1,
	 output reg [16:0] Data_Output_2,
	 output reg [16:0] Data_Output_3,
	 output reg [16:0] Data_Output_4,
	 output reg [16:0] Data_Output_5,
	 output reg [16:0] Data_Output_6,
	 output reg [16:0] Data_Output_7,
	 output reg [16:0] Data_Output_8,
	 output reg [16:0] Data_Output_9,
	 output reg [16:0] Data_Output_10,
	 output reg [16:0] Data_Output_11,
	 output reg [16:0] Data_Output_12,
	 output reg [16:0] Data_Output_13,
	 output reg [16:0] Data_Output_14,
	 output reg [16:0] Data_Output_15
    );
	 
initial begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0;
end

always@(sel) begin
 case (sel) 
  0:begin
Data_Output_0=Data_Input;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  1:begin
  Data_Output_0=0;
Data_Output_1=Data_Input;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  2:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=Data_Input;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  3:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=Data_Input;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  4:begin
 Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=Data_Input;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
end
  5:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=Data_Input;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  6:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=Data_Input;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  7:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=Data_Input;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  8:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=Data_Input;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  9:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=Data_Input;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  10:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=Data_Input;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  11:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=Data_Input;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  12:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=Data_Input;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=0; 
	end
  13:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=Data_Input;
Data_Output_14=0;
Data_Output_15=0; 
	end
  14:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=Data_Input;
Data_Output_15=0; 
	end
  15:begin
Data_Output_0=0;
Data_Output_1=0;
Data_Output_2=0;
Data_Output_3=0;
Data_Output_4=0;
Data_Output_5=0;
Data_Output_6=0;
Data_Output_7=0;
Data_Output_8=0;
Data_Output_9=0;
Data_Output_10=0;
Data_Output_11=0;
Data_Output_12=0;
Data_Output_13=0;
Data_Output_14=0;
Data_Output_15=Data_Input;
	end
endcase
end
endmodule

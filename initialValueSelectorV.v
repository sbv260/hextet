`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company: Universidad de Guadalajara
// Engineer: Ing. Sergio Barrios
// 
// Create Date:    17:26:21 10/17/2015 
// Design Name:    Selector de valor inicial
// Module Name:    initialValueSelector 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:   Multiplexor sobre varios valores constantes.
//
// Dependencies: Ninguna.
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module initialValueSelectorV(state,A,U,V);
	 parameter n = 16;
    input wire state;
    input wire [6:0] A;
	 input wire [n-1:0] U;
    output reg [n-1:0] V;
    
	 wire [3:0] A_i;
	 
	 assign A_i = (state)?A[6:3]:{1'b0,A[2:0]};
	 
always @ (A_i,U)
begin
	case(A_i)
		4'd00: V = 16'b0000000000000001;
		4'd01: V = 16'b0000000000000010;
		4'd02: V = 16'b0000000000010000;
		4'd03: V = 16'b0000000001000000;
		4'd04: V = 16'b0000001000000000;
		4'd05: V = 16'b0000100000000000;
		4'd06: V = 16'b0100000000000000;
		4'd07: V = 16'b1000000000000000;
		4'd08: V = U;
		4'd09: V = {7'b0,U[15]|U[14]|U[13]|U[12]|U[11]|U[10]|U[9]|U[8],7'b0,U[7]|U[6]|U[5]|U[4]|U[3]|U[2]|U[1]|U[0]};
//		4'd10: V = 16'b0000000000010000;
//		4'd11: V = 16'b0000000001000000;
//		4'd12: V = 16'b0000001000000000;
//		4'd13: V = 16'b0000100000000000;
//		4'd14: V = 16'b0010000000000000;
//		4'd15: V = 16'b1000000000000000;
		default: V = 16'b0000000000000001;
	endcase
end

endmodule

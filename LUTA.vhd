----------------------------------------------------------------------
-- Company:        Universidad de Guadalajara
-- Engineer:       Ing. Sergio Barrios
-- 
-- Create Date:    00:27:04 03/19/2015 
-- Design Name:    LUT A
-- Module Name:    LUTA - Structural 
-- Project Name:   RDN
-- Target Devices: Spartan 3E
-- Tool versions: 
-- Description: La salida es igual a la entrada.
--
-- Dependencies: Ninguna.
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity LUTA is
	 Generic (Y : integer := 0);
    Port ( Din : in  STD_LOGIC;
           Dout : out  STD_LOGIC);
end LUTA;

architecture Structural of LUTA is
ATTRIBUTE RLOC : string;
ATTRIBUTE RLOC of LUT1_instA : label is "X0Y"&INTEGER'image(Y);
signal Dout_i : STD_LOGIC;
begin
-- LUT1: 1-input Look-Up Table with general output
-- Virtex-6
-- Xilinx HDL Libraries Guide, version 13.3
LUT1_instA : LUT1
generic map (
INIT => "01")
port map (
O => Dout_i, -- LUT general output
I0 => Din -- LUT input
);
-- End of LUT1_inst instantiation
Dout <= transport Dout_i after 1.25 ns;
end Structural;


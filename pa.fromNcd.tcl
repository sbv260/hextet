
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name AER_8bit -dir "C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/planAhead_run_2" -part xc7z020clg400-2
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk} }
set_property target_constrs_file "top.ucf" [current_fileset -constrset]
add_files [list {top.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/top.ncd"
if {[catch {read_twx -name results_1 -file "C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/top.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/top.twx\": $eInfo"
}

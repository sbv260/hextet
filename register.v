`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company:       Universidad de Guadalajara
// Engineer:      Ing. Sergio Barrios
// 
// Create Date:    19:00:46 02/03/2016 
// Design Name:    Registro
// Module Name:    register 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:    Registro para salvar la configuracion de la neurona
//
// Dependencies:   Ninguna
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module register(
    input wire clk,
    input wire cen,
    input wire [15:0] D,
    output reg [15:0] Q
    );
	 initial begin
		Q <= 0;
	 end
	 
    always @(posedge clk)
    begin
        if (cen) begin
				Q <= D;
		  end else begin
				Q <= Q;
		  end
	 end
		  
endmodule

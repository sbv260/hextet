`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company: Universidad de Guadalajara
// Engineer: Ing. Sergio Barrios
// 
// Create Date:    17:26:21 10/17/2015 
// Design Name:    Selector de valor inicial
// Module Name:    initialValueSelector 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:   Multiplexor sobre varios valores constantes.
//
// Dependencies: Ninguna.
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module initialValueSelectorU(state,A,U);
	 parameter n = 16;
    input wire state;
    input wire [6:0] A;
    output reg [n-1:0] U;
    
	 wire [3:0] A_i;
	 
	 assign A_i = (state)?A[6:3]:{1'b0,A[2:0]};
	 
always @ (A_i)
begin
	case(A_i)
		4'd00: U = 16'b0000000000000001;
		4'd01: U = 16'b0000000000000010;
		4'd02: U = 16'b0000000000000100;
		4'd03: U = 16'b0000000000001000;
		4'd04: U = 16'b0000000000010000;
		4'd05: U = 16'b0000000000100000;
		4'd06: U = 16'b0000000001000000;
		4'd07: U = 16'b0000000010000000;
		4'd08: U = 16'b0000000100000000;
		4'd09: U = 16'b0000001000000000;
		4'd10: U = 16'b0000010000000000;
		4'd11: U = 16'b0000100000000000;
		4'd12: U = 16'b0001000000000000;
		4'd13: U = 16'b0010000000000000;
		4'd14: U = 16'b0100000000000000;
		4'd15: U = 16'b1000000000000000;
		default: U = 16'b0000000000000001;
	endcase
end

endmodule

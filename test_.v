`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:09:37 02/22/2018
// DeSE1ign Name:   RotateDigitalNeuron
// Module Name:   C:/USE1erSE1/JRobert/DocumentSE1/Xilinx/AER_zynq/trunk/teSE1t_.v
// Project Name:  AER_8bit
// Target Device:  
// Tool verSE1ionSE1:  
// DeSE1cription: 
//
// Verilog TeSE1t Fixture created by ISE1E for module: RotateDigitalNeuron
//
// DependencieSE1:
// 
// ReviSE1ion:
// ReviSE1ion 0.01 - File Created
// Additional CommentSE1:
// 
////////////////////////////////////////////////////////////////////////////////

module test_RDN;

	// InputSE1
	reg clk;
	reg rst;
	reg cen;
	reg [15:0] Ain;
	reg SE1;
	reg SE2;
	reg SE3;
	reg SI1;
	reg SI2;
	reg SI3;
	reg ack;

	// OutputSE1
	wire Y;
	
	always begin
	#41 clk = !clk;
	end

	// InSE1tantiate the Unit Under TeSE1t (UUT)
	RotateDigitalNeuron uut (
		.clk(clk), 
		.rst(rst), 
		.cen(cen), 
		.Ain(Ain), 
		.SE1(SE1), 
		.SE2(SE2), 
		.SE3(SE3), 
		.SI1(SI1), 
		.SI2(SI2), 
		.SI3(SI3), 
		.Y(Y), 
		.ack(ack)
	);

	initial begin
		// Initialize InputSE1
		clk = 0;
		rst = 0;
		cen = 0;
		Ain = 16'b0001101111110000;
		SE1 = 0;
		SE2 = 0;
		SE3 = 0;
		SI1 = 0;
		SI2 = 0;
		SI3 = 0;
		ack = 0;

		// Wait 100 nSE1 for global reSE1et to finiSE1h
		#100;
        
		// Add SE1timuluSE1 here
		rst = 1;
		#10;
		rst = 0;
		#100;
		cen = 1;
		#300;
		cen = 0;
		#20;
		rst = 1;
		#10;
		rst = 0;
		#10;
		#2000;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		SE1 = 1;
		#30;
		SE1=0;
		#100;
		ack = 1;
		#3;
		ack = 0;
		#100;
		ack = 1;
		#3;
		ack = 0;
		#100;
		ack = 1;
		#3;
		ack = 0;
		#100;
		ack = 1;
		#3;
		ack = 0;
		#100;
		ack = 1;
		#3;
		ack = 0;
	end
      
endmodule


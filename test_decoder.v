`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:00:08 11/07/2017
// Design Name:   decoder
// Module Name:   C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/test_decoder.v
// Project Name:  AER_8bit
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: decoder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_decoder;

	// Inputs
	reg clk;
	reg [6:0] data;
	reg R;

	// Outputs
	wire [127:0] output_channels;
	wire A;

	// Instantiate the Unit Under Test (UUT)
	decoder uut (
		.clk(clk), 
		.output_channels(output_channels), 
		.data(data), 
		.R(R), 
		.A(A)
	);
	
	always begin
	#41 clk = !clk;
	end
	
	initial begin
		// Initialize Inputs
		clk = 0;
		data = 0;
		R = 0;

		// Wait 100 ns for global reset to finish
		#100;
		data = 4'b1000;
		#10;
		R = 1;
		#30;
		R = 0;
        
		// Add stimulus here

	end
      
endmodule


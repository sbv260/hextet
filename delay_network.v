`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    05:43:44 11/01/2017 
// Design Name: 
// Module Name:    delay_network 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module delay_network(
    input wire clk,
    output wire [15:0] clk_array
    );

assign clk_array[0] = clk;
delay_for_network delay1 (clk_array[0],clk_array[1]);
delay_for_network delay2 (clk_array[1],clk_array[2]);
delay_for_network delay3 (clk_array[2],clk_array[3]);
delay_for_network delay4 (clk_array[3],clk_array[4]);
delay_for_network delay5 (clk_array[4],clk_array[5]);
delay_for_network delay6 (clk_array[5],clk_array[6]);
delay_for_network delay7 (clk_array[6],clk_array[7]);
assign clk_array[8] = !clk;
delay_for_network delay9 (clk_array[8],clk_array[9]);
delay_for_network delay10 (clk_array[9],clk_array[10]);
delay_for_network delay11 (clk_array[10],clk_array[11]);
delay_for_network delay12 (clk_array[11],clk_array[12]);
delay_for_network delay13 (clk_array[12],clk_array[13]);
delay_for_network delay14 (clk_array[13],clk_array[14]);
delay_for_network delay15 (clk_array[14],clk_array[15]);


endmodule

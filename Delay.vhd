----------------------------------------------------------------------
-- Company: 		Universidad de Guadalajara
-- Engineer:        Ing. Sergio Barrios
-- 
-- Create Date:    02:42:30 03/19/2015 
-- Design Name:    Pequeno retraso instanciando LUT separadas
-- Module Name:    Delay - Structural 
-- Project Name:   RDN
-- Target Devices: Spartan 3E
-- Tool versions: 
-- Description: Instancia dos LUTs separadas una cantidad
--				variable de posiciones
-- Dependencies: LUTA, LUTB 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity transportDelay is
	 Generic (X : integer := 150;
				 Y : integer := 0--;
				 --USET : string := "0");
				 );
    Port ( D : in  STD_LOGIC;
           Dout : inout  STD_LOGIC);
end transportDelay;

architecture Structural of transportDelay is
component LUTA is
	 Generic (Y : integer := 0);
    Port ( Din : in  STD_LOGIC;
           Dout : out  STD_LOGIC);
end component;
component LUTB is
	 Generic (X : integer := 0;
				 Y : integer := 0);
    Port ( Din : in  STD_LOGIC;
           Dout : out  STD_LOGIC);
end component;

signal d_l: STD_LOGIC:= '0';

attribute keep : string;
attribute keep of d_l : signal is "TRUE";
attribute U_SET : string;
--attribute U_SET of LTA: label is "USET:"&USET;
--attribute U_SET of LTB: label is "USET:"&USET;
begin
LTA: LUTA generic map (Y => Y) port map (Din => D, Dout => d_l);
LTB: LUTB generic map (X => X,Y => Y) port map (Din => d_l, Dout => Dout); 

end Structural;


`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   05:54:25 11/01/2017
// Design Name:   delay_network
// Module Name:   C:/Users/JRobert/Documents/Xilinx/AER_zynq/trunk/test_delay_network.v
// Project Name:  AER_8bit
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: delay_network
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_delay_network;

	// Inputs
	reg clk;

	// Outputs
	wire [15:0] clk_array;

	// Instantiate the Unit Under Test (UUT)
	delay_network uut (
		.clk(clk), 
		.clk_array(clk_array)
	);
	
	always begin
	#41 clk = !clk;
	end

	initial begin
		// Initialize Inputs
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule


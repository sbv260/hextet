`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company:       Universidad de Guadalajara
// Engineer:      Ing. Sergio Barrios
// 
// Create Date:    13:29:20 07/14/2015 
// Design Name:    Retraso
// Module Name:    delay 
// Project Name:   RDN
// Target Devices: Spartan 3E
// Tool versions: 
// Description:    Instancia un 15 retrasos descritos en VHDL.
//
// Dependencies:   multipleDelay
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module delay(
    input wire di,
    output wire Do
    );
//parameter uset = "0";
//multipleDelay #(.N(15),.X(15),.USET(uset)) DELA (
multipleDelay #(.N(15),.X(15)) DELA (
           .D(di),
           .Dout(Do));

endmodule

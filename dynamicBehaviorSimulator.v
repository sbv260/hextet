`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////
// Company: Universidad de Guadalajara
// Engineer: Ing. Sergio Barrios
// 
// Create Date:    18:01:59 10/17/2015 
// Design Name:    Simulador de comportamiento dinamico
// Module Name:    dynamicBehaviorSimulator 
// Project Name:   RDN
// Target Devices: 
// Tool versions: Spartan 3E
// Description:   Maquina de estados finita
//
// Dependencies: Ninguna
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////
module dynamicBehaviorSimulator(rst,clk,U,V,A,S,state,newstate,rstU,rstV);
	 parameter n = 16;
	 input wire rst;
	 input wire clk;
	 input wire [n:0] U;
	 input wire [n:0] V;
	 input wire  [1:0] A;
	 input wire S;
    output wire rstU;
	 output wire rstV;
    output wire newstate;

	 output reg state;
	 reg newstate_i;
	 wire goActive, goResting;
	 wire changestate;
	 wire statechanged;
	 
	 
initial begin
	state = 0;
	newstate_i = 0;
end

assign newstate = newstate_i;
assign goActive = (!state)&&((V[n]&&(!A[0]))||(S&&U[n-1]&&A[0]));
assign goResting = (state)&&((U[n-1]&&(!A[1]))||(S&&(V[n]||V[n-1])&&A[1]));
assign changestate = goActive||goResting;
assign  rstV  =  rst||statechanged;
assign  rstU  =  U[n]||(S&&A[0])||(S&&!A[0]&&!newstate_i)||(S&&!A[0]&&state)||rst||statechanged;	

always @ (posedge changestate)
begin
	newstate_i = !state;
end

always @ (posedge clk,posedge rst) 
begin
	if (rst) begin
		state = 0;
	end else begin
		state = newstate_i;
	end
end

delay delayStateChanged((state^newstate_i),statechanged);

endmodule

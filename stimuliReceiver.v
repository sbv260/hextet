`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:19:14 06/19/2017 
// Design Name: 
// Module Name:    stimuliReceiver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module stimuliReceiver(
    input wire SE1,
	 input wire SE2,
	 input wire SE3,
    input wire SI1,
	 input wire SI2,
	 input wire SI3,
    output wire S,
    input wire dvi,
    output wire dvo
    );
	 
	 wire SE,SI;
	
DendT denE (SE1,SE2,SE3,SE);
DendT denI (SI1,SI2,SI3,SI);
assign S = SE^SI;
assign dvo = (SI&&!SE)?!dvi:dvi;
endmodule
